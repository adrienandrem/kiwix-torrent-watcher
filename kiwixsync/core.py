# -*- coding: utf-8 -*-
"""
Created on Sun May 17 11:52:03 2020

@author: @author: Adrien André <adr.andre@laposte.net>
"""
import os
import re

__all__ = ["Zim_File"]


class Zim_File(object):

    def __init__(self, website, lang, selection, content, publication):
        self.website = website
        self.lang = lang
        self.selection = selection
        self.content = content
        self.publication = publication

    # http://download.kiwix.org/zim/wikipedia_en_all_nopic_2020-03.zim.torrent

    def __repr__(self):
        return str({"website": self.website, "lang": self.lang,
                    "selection": self.selection,
                    "content": self.content, "publication": self.publication})

    def basename(self):
        """File name"""
        return '_'.join([self.website, self.lang, self.selection, self.content])

    def fullname(self):
        """File name with version date"""
        return "%s_%s" % (self.basename(), self.publication)

    def __lt__(self, other):
        return self.fullname() < other.fullname()

    def to_update(self, other):
        return other.publication > self.publication

    def torrent(self, server):
        return "%s/zim/%s.zim.torrent" % (server, self.basename())

    @staticmethod
    def from_path(path):
        """Builds Zim_File instance from path
        ex.: zim/wikipedia_en_all_nopic_2020-03.zim"""
        base = os.path.basename(path)
        name, __ = os.path.splitext(base)  # wikipedia_en_all_nopic_2020-03.zim

        website, lang, selection, content, publication = re.split('_', name)

        return Zim_File(website, lang, selection, content, publication)
